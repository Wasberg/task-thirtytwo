import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor() { }

  onRegisterAttempted(registerResult: any): void {
    alert(registerResult.message);
  }

  ngOnInit(): void {
  }

}
